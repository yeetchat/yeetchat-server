# YeetChat

_This is the server-repository, and as such, the server-repo-oriented readme_

## Typescript Notice

If you see a .ts and a .js file in the same folder, with the same name, ONLY edit the .ts file; the .js file will be replaced by someone while building the TypeScript file eventually.
